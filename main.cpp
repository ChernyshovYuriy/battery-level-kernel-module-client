#include <iostream>
#include <sys/socket.h>
#include <linux/netlink.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>

/**
 * The netlink protocol number.
 */
static const int NETLINK_USER = 31;
/**
 * Maximum payload size.
 */
static const int MAX_PAYLOAD = 1024;

int main() {
    const int sock_fd = socket(PF_NETLINK, SOCK_RAW, NETLINK_USER);
    if (sock_fd < 0) {
        printf(
                "Can not create socket:%d\n",
                sock_fd
        );
        return -1;
    }

    struct sockaddr_nl src_addr {};
    memset(&src_addr, 0, sizeof(src_addr));
    src_addr.nl_family = AF_NETLINK;
    // Self pid.
    src_addr.nl_pid = getpid();

    bind(sock_fd, (struct sockaddr *) &src_addr, sizeof(src_addr));

    for (int i = 0; i < 10; i++) {
        struct sockaddr_nl dest_addr{};
        memset(&dest_addr, 0, sizeof(dest_addr));
        dest_addr.nl_family = AF_NETLINK;
        // For Linux Kernel.
        dest_addr.nl_pid = 0;
        // unicast
        dest_addr.nl_groups = 0;

        auto *nlh = (struct nlmsghdr *) malloc(NLMSG_SPACE(MAX_PAYLOAD));
        memset(nlh, 0, NLMSG_SPACE(MAX_PAYLOAD));
        nlh->nlmsg_len = NLMSG_SPACE(MAX_PAYLOAD);
        nlh->nlmsg_pid = getpid();
        nlh->nlmsg_flags = 0;

        strcpy((char *)NLMSG_DATA(nlh), "<Message to Kernel>");

        struct iovec iov{};
        memset(&iov, 0, sizeof(iov));
        iov.iov_base = (void *) nlh;
        iov.iov_len = nlh->nlmsg_len;

        static struct msghdr msg;

        memset(&msg, 0, sizeof(msg));
        msg.msg_name = (void *) &dest_addr;
        msg.msg_namelen = sizeof(dest_addr);
        msg.msg_iov = &iov;
        msg.msg_iovlen = 1;

        printf("Sending message to kernel\n");
        sendmsg(sock_fd, &msg, 0);
        printf("Waiting for message from kernel\n");

        recvmsg(sock_fd, &msg, 0);
        printf("Received message: %s\n", (char *) NLMSG_DATA(nlh));

        sleep(1);
    }

    close(sock_fd);
    return 0;
}
